![](https://gitlab.com/Antoniii/bessel-van-python/-/raw/main/0.PNG)

![](https://gitlab.com/Antoniii/bessel-van-python/-/raw/main/01.PNG)

![](https://gitlab.com/Antoniii/bessel-van-python/-/raw/main/2.PNG)

![](https://gitlab.com/Antoniii/bessel-van-python/-/raw/main/Figure_1.png)

![](https://gitlab.com/Antoniii/bessel-van-python/-/raw/main/1.PNG)

![](https://gitlab.com/Antoniii/bessel-van-python/-/raw/main/Figure_3.png)

![](https://gitlab.com/Antoniii/bessel-van-python/-/raw/main/3.PNG)

## Sources

* [Функции Бесселя в программе символьной математики SymPy](https://habr.com/ru/post/443628/)
* Фаддеева В. Н., Гавурин М. К. «Таблицы функций Бесселя целых номеров от 0 до 120»
