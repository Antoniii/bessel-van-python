# Функции Бесселя первого рода
# from sympy import*
# from sympy.plotting import plot
# x,n, p=var('x,n, p')
# def besselj(p,x):
#     return summation(((-1)**n*x**(2*n+p))/(factorial(n)*gamma(n+p+1)*2**(2*n+p)),[n,0,oo])
# st="J_{p}(x)"
# p1=plot(besselj(0,x),(x,-20,20),line_color='b',title=' $'+st+ '$',show=False)    
# p2=plot(besselj(1,x),(x,-20,20),line_color='g',show=False)  
# p3=plot(besselj(2,x),(x,-20,20),line_color='r',show=False)
# p4=plot(besselj(3,x),(x,-20,20),line_color='c',show=False)
# p1.extend(p2)
# p1.extend(p3)
# p1.extend(p4)
# p1.show()


# Свойство функции Бесселя первого рода
# from sympy import*
# from sympy.plotting import plot
# x,n, p=var('x,n, p')
# def besselj(p,x):
#     return summation(((-1)**n*x**(2*n+p))/(factorial(n)*gamma(n+p+1)*2**(2*n+p)),[n,0,oo])
# st="J_{1}(x)=-J_{-1}(x)"
# p1=plot(besselj(1,x),(x,-10,10),line_color='b',title=' $'+st+ '$',show=False)    
# p2=plot(besselj(-1,x),(x,-10,10),line_color='r',show=False)  
# p1.extend(p2)
# p1.show()


# Функция дробного порядка и её производная
# from sympy import*
# from sympy.plotting import plot
# x,n, p=var('x,n, p')
# def besselj(p,x):
#     return summation(((-1)**n*x**(2*n+p))/(factorial(n)*gamma(n+p+1)*2**(2*n+p)),[n,0,oo])
# st="J_{1/3}(x),J{}'_{1/3}(x)"
# p1=plot(besselj(1/3,x),(x,-1,10),line_color='b',title=' $'+st+ '$',ylim=(-1,2),show=False)
# def dbesselj(p,x):
#     return diff(summation(((-1)**n*x**(2*n+p))/(factorial(n)*gamma(n+p+1)*2**(2*n+p)),[n,0,oo]),x)
# p2=plot(dbesselj(1/3,x),(x,-1,10),line_color='g',show=False)  
# p1.extend(p2)
# p1.show()


# Функция Бесселя первого рода $J_{N}(x)$ для 
# положительных целых порядков n = 0,1,2,3 — решение уравнения Бесселя:
# from mpmath import*
# j0 = lambda x: besselj(0,x)
# j1 = lambda x: besselj(1,x)
# j2 = lambda x: besselj(2,x)
# j3 = lambda x: besselj(3,x)
# plot([j0,j1,j2,j3],[0,14])




# Функция $besselj(N,x)$ обеспечивает результат
#  с заданным числом цифр $(mp.dps)$ после запятой:
from mpmath import*
mp.dps = 15; mp.pretty = True
# print(besselj(2, 1000))
# nprint(besselj(4, 0.75))
# nprint(besselj(2, 1000j))
# mp.dps = 25
# nprint( besselj(0.75j, 3+4j))
# mp.dps = 50
# nprint( besselj(1, pi))

"В.Н. Фаддеева и М.К. Гавурин Таблицы функций Бесселя J_n(x) целых номеров от 0 до 120"

n = int(input("n = ")) 
x = float(input("x = "))
print(besselj(n, x))

"Нули функции Бесселя"
# Для реального порядка $\mathit{\nu}\geq 0$ и 
# положительного целого числа $m$ возвращает
#  $j_{\nu,m}$, m-й положительный нуль функции Бесселя
#   первого рода $J_{\nu}(z)$

# from mpmath import *
# mp.dps = 25; mp.pretty = True
v = float(input("v = ")) 
n = int(input("n = "))
print(besseljzero(v,n))